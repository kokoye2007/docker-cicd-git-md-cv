# CV

## Curriculum Vitae / Resume

## Markdown Editor

### Markdown Online Editor

* [StackEdit](https://stackedit.io/app)

* [Dillinger](https://dillinger.io/)

* [JBT Markdown](https://jbt.github.io/markdown-editor/)


### Markdown Editor Download

* [DocumentNode](https://documentnode.io/)

* [Typora](https://www.typora.io/#)


## Latex Editor

### Latex Online Editor

* [OverLeaf](https://www.overleaf.com)

* [ShareLatex](https://www.sharelatex.com)

* [papeeria](https://www.papeeria.com/)

### Latex CV Templates

* [Overleaf](https://www.overleaf.com/gallery/tagged/cv)

* [ShareLatex](https://www.sharelatex.com/templates/cv-or-resume) 

* [LatexTemplates](https://www.latextemplates.com/cat/curricula-vitae)

* [zety](https://zety.com/blog/latex-resume-template)

* [Orbit-CV](https://github.com/mvarela/orbit-cv)


## Markdown to Latex Converter

### Online Converter

* [CloudConvert](https://cloudconvert.com/md-to-tex)


### Convert with Pandoc

* [pandoc](https://pandoc.org/demos.html)

#### Markdown to PDF 

```
pandoc -s your-markdown.md -o your-pdf.pdf
```


#### Markdown to Latex

```
pandoc -s  -t context your-markdown.md -o your-latex.tex
```
#### Latex to PDF

```
pandoc your-latex.tex --pdf-engine=xelatex -o your-pdf.pdf
```

## Gitlab CI 

```
make_pdf:
  image:  moss/xelatex
  script:
    - latexmk -pdf  your-latex-cv.tex
  artifacts:
    paths:
      -  your-latex-cv.pdf
```

#### feedback me ;)
